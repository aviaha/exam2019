import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from "@angular/router";

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
email='';
password='';
nickname='';
error='';
name;

  constructor(private auth:AuthService,private router:Router) { }
signUp(){
  this.auth.signup(this.email,this.password).
  then(value =>{
    this.auth.updateProfile(value.user,this.nickname);
    this.auth.addUser(value.user, this.name);
    //console.log(value)}).then(value=>{
      this.router.navigate(['/books']);
    }).catch(err=>{
      this.error =err;;
      
    })
}

login(){
  this.auth.login(this.email, this.password)
  .then(user=>{
    this.router.navigate(['/books']);
  }).catch(err=>{
    this.error = err;
    console.log (err);
  })
}
  ngOnInit() {
  }

}
