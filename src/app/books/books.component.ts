import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database'
import {BooksService} from '../books.service';


@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  
  books =[];
  namebook:string;

  constructor(public auth:AuthService, private router:Router,private db:AngularFireDatabase, private booksService:BooksService) { }
  logout(){
    this.auth.logout().then(value =>{
      this.router.navigate(['login'])
    }).catch(err=>{
      //console.log(err);
    })
  }

  login(){
    this.router.navigate(['/books'])
  }
  addBook(  text:string){
    this.booksService.addBook(this.namebook);
    this.namebook ='';
  }

  ngOnInit() {
    this.db.list('/books').snapshotChanges().subscribe(
      books =>{
        this.books =[];
        books.forEach(
          book =>{
            let y = book.payload.toJSON();
            y['$key'] = book.key;
            this.books.push(y);
          }
        )
      }
    )

  
}

}
