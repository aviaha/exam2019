import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class BooksService {
  
  delete(key){
    this.authService.user.subscribe(user=>{
    this.db.list('/users/'+user.uid+'/books').remove(key);
  })}
  
  addBook(namebook){
    this.authService.user.subscribe(user=>{
      this.db.list('/books/'+user.uid+'/books').push({'namebook':namebook});
    })
    
  }
  
  
  constructor(private authService:AuthService,
              private db:AngularFireDatabase  ) { }
}