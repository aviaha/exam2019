import { Component, OnInit, Input, Output,EventEmitter} from '@angular/core';
import { BooksService } from '../books.service';


@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();

  namebook;
  author;
  key;
  checked:boolean;

  
  showTheButton = false;
  update(){
    this.key.update(this.key,this.checked)
  }
  send(){
    this.myButtonClicked.emit(this.namebook);
  }
  showButton(){
    this.showTheButton =true;
  }
  hideButton(){
    this.showTheButton =false;
  }
  delete(){
  this.booksService.delete(this.key)
  }

  constructor(private  booksService: BooksService) { }

  ngOnInit() {

    this.namebook=this.data.namebook;
    this.author=this.data.author;
    this.key=this.data.$key;
  }

}
