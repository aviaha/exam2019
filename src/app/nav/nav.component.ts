import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(public Auth:AuthService, private router:Router) { }
  logout(){
    this.Auth.logout()
  }
  login(){
    this.router.navigate(['/login'])
  }

  ngOnInit() {
  }
 // toRegister(){
   // this.router.navigate(['register'])
  //}
  //toBooks(){
  //  this.router.navigate(['books'])
 // }

}
