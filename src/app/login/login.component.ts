import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
email='';
password='';
error='';
  constructor(private auth:AuthService,private router:Router) { }
  ngOnInit() {
  }
  
  login(){
    this.auth.login(this.email, this.password)
    .then(user=>{
      this.router.navigate(['/books']);
    }).catch(err=>{
      this.error = err;
      console.log (err);
    })
  }
}

